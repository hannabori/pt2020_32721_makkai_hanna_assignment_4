package DataProcessing;

import DataAccess.TxtWriter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task {
    private List<MonitoredData> monitoredData;

    private static SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Task(List<MonitoredData> monitoredData) {
        this.monitoredData = monitoredData;
    }

    //TASK 2
    public void numberOfDays() {
        int days = monitoredData.stream()
                .collect(Collectors.groupingBy(p -> {
                    try {
                        return format.parse(p.getStart_time()).getTime() / (1000 * 3600 * 24);
                    } catch (ParseException | NullPointerException e) {
                        e.printStackTrace();
                    }
                    return null;
                })).size();

        TxtWriter.write("Task_2.txt", "Days monitorized: " + days);
    }

    //TASK 3
    public void countOfActivities(){
        Map<String, Integer> numberOfActivityAppearance;
        numberOfActivityAppearance =  monitoredData.stream().collect(Collectors.toMap(s -> s.getActivity(), s -> 1, Integer::sum));
        TxtWriter.write("Task_3.txt", "Occurrence of each activity: \n\n" + numberOfActivityAppearance.toString());
    }

    //TASK 4
    public void countOfActivitiesDaily(){
        Map<Integer, Map<String, Integer>> numberOfActivitiesDaily;
        numberOfActivitiesDaily = monitoredData.stream()
                .collect(Collectors.groupingBy((p -> {
                    try {
                        return Integer.parseInt(String.valueOf((format.parse(p.getStart_time()).getTime()/(1000 * 3600 * 24))));
                    } catch (ParseException | NullPointerException e) {
                        e.printStackTrace();
                    }
                    return null;
                }), Collectors.toMap(MonitoredData::getActivity, p -> 1, Integer::sum)));

       TxtWriter.write("Task_4.txt", "Number of activities daily:\n\n" + numberOfActivitiesDaily.toString());
    }

    //TASK 5
    public void activitiesDuration() {

        TxtWriter.write("Task_5.txt", "Duration of each activity in minutes in order:\n\n");
        monitoredData.stream().map(p -> {
                    int diff = 0;
                    try {
                        diff = Integer
                                .parseInt(String.valueOf((format.parse(p.getEnd_time()).getTime()
                                        - format.parse(p.getStart_time()).getTime()) / (1000 * 60)));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String result = p.getActivity();
                    result += " " + diff;
                    return result;
                }
        ).forEach(p -> TxtWriter.write("Task_5.txt", p));

   }

    //TASK 6
    public void totalDurationOfActivities() {
        Map<String, Integer> totalDurationOfActivities;
        totalDurationOfActivities = monitoredData.stream()
                .collect(Collectors.toMap(MonitoredData::getActivity, p -> {
                    try {
                        return Integer.parseInt(String.valueOf((format.parse(p.getEnd_time()).getTime() - format.parse(p.getStart_time()).getTime())/(1000 * 60)));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return null;
                }, Integer::sum));
        TxtWriter.write("Task_6.txt", "Total duration of every activity in minutes:\n\n" + totalDurationOfActivities.toString());
    }

    //TASK 7
    public void shortActivites(){
        List<String> activities = null;
        /*activities = monitoredData.stream()
                .filter(p -> {
                    try {
                        return Integer.parseInt(String
                                .valueOf((format.parse(p.getEnd_time()).getTime() - format.parse(p.getStart_time()).getTime())/(1000 * 60)<5);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return null;
                        }
                    ).collect(Collectors.toMap(MonitoredData::getActivity, s->1, Integer::sum));
                    //
                    */

        TxtWriter.write("Task_7.txt", "Usually short activities:\n\n" + activities);
    }

}
