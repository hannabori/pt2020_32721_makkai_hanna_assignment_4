package DataProcessing;

public class MonitoredData {
    String start_time;
    String end_time;
    String activity;

    public MonitoredData(String start_time, String end_time, String activity){
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity = activity;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getEnd_time(){
        return end_time;
    }

    public String getActivity(){
        return activity;
    }

    public void setStart_time(String start_time){
        this.start_time = start_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "Activity: " + activity +
                "started at: " + start_time +
                ", ended at: " + end_time +
                "\n";
    }
}
