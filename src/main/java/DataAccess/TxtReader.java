package DataAccess;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import DataProcessing.MonitoredData;
import java.util.stream.Stream;

public class TxtReader {

    static TxtWriter writer = new TxtWriter();

    public static void parseValues(String lineOfData, ArrayList<MonitoredData> monitoredData) {
        String[] splitter = lineOfData.split("\t\t");
        splitter[2] = splitter[2].replaceAll("\\s+", "");
            monitoredData.add(new MonitoredData(splitter[0], splitter[1], splitter[2]));
            //System.out.println(lineOfData);
        writer.write("Task_1.txt", lineOfData);
    }

    //TASK 1
    public ArrayList<MonitoredData> readData(String fileName) {
        ArrayList<MonitoredData> monitoredData = new ArrayList<>();
        try (Stream<String> rows = Files.lines(Paths.get(fileName))) {
            rows.forEachOrdered(row -> parseValues(row, monitoredData));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return monitoredData;
    }

    public TxtReader(){
        readData("Activities.txt");
    }
}
