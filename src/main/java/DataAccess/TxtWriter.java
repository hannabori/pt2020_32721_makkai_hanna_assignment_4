package DataAccess;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TxtWriter {
    public static void write(String fileName, String data){
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write(data + "\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Couldn't write to file! :(");
        }
    }

    public TxtWriter() {}
}
