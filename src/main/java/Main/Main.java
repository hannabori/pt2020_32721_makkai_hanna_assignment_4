package Main;

import DataAccess.TxtReader;
import DataProcessing.Task;
import DataProcessing.MonitoredData;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        TxtReader txtReader = new TxtReader();
        //TASK 1
        ArrayList<MonitoredData> monitoredData = txtReader.readData("Activities.txt");
        Task task = new Task(monitoredData);

        //TASK 2
        task.numberOfDays();

        //TASK 3
        task.countOfActivities();

        //TASK 4
        task.countOfActivitiesDaily();

        //TASK 5
        task.activitiesDuration();

        //TASK 6
        task.totalDurationOfActivities();
    }
}
